(ns plf08.cifrado2
  (:require [plf08.tablas :as table]
            [clojure.string]))
(defn posiciones
  [xs]
  (into [] (map (fn [c] (if (> (table/tabla-columnar c) 75)
                          (- (table/tabla-columnar c) 75)
                          (table/tabla-columnar c)))
                xs)))

(defn comprobacion-matriz
  [xs acc ys]
  (if (int? (/ (count ys) (count xs)))
    (into [] (map vec (partition-all acc ys)))
    (conj (into [] (drop-last (partition-all acc ys)))
          (concat (last (partition-all acc ys)) (repeat (count xs) "")))))

(defn transpuesta
  [xs ys]
  (into [] (apply map vector (comprobacion-matriz xs (count xs) ys))))

(defn ordenar
  [xs ys]
  (keys (sort-by val < (zipmap (transpuesta xs ys)
                               (posiciones xs)))))

(defn comprobacion
  [xs]
  (into [] (map (fn [c] (table/tabla-columnar c)) xs)))

(ns plf08.core
  (:gen-class)
  (:require [plf08.cifrado2 :as c2]
            [plf08.archivo :as file]
            [plf08.pares :as p]
            [clojure.string :as string]))

(defn cifrar-polibio
  [r s]
  (file/escribir-archivo s (string/join (map p/tabla (file/leer-archivo r)))))

(defn descifrar-polibio
  [r s]
  (file/escribir-archivo s (string/join (map p/tabla (vec (map (partial apply str)
                                                               (partition-all 2 (file/leer-archivo r))))))))
(defn cifrar-transposición
  [xs ys zs]
  (file/escribir-archivo zs (cond (or (empty? xs) (empty? (file/leer-archivo ys))) "Dato vacio"
                                  (some nil? (c2/comprobacion-matriz xs)) "Caracter desconocido"
                                  :else (apply str (flatten (c2/ordenar xs (file/leer-archivo ys)))))))

(defn descifrado-transposición
  [xs ys zs]
  (file/escribir-archivo zs (apply str (flatten (apply map vector
                                                       (keys (sort-by val < (zipmap (c2/comprobacion-matriz xs (inc (count xs)) (file/leer-archivo ys))
                                                                                    (c2/posiciones xs)))))))))

(defn -main
  ([a b c] [(if (= a "cifrar")
              (cifrar-polibio b c)
              (descifrar-polibio b c))])
  ([a b c d] [(if (= a "cifrar")
                (cifrar-transposición b c d)
                (descifrado-transposición b c d))]))
